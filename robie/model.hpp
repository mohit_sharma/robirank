/*
 * Copyright (c) 2014 Hyokun Yun
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */
#ifndef __SOLAR_MODEL_HPP
#define __SOLAR_MODEL_HPP

#include <iostream>
#include <queue>
#include <map>
#include <unordered_set>
#include <set>
#include <sstream>

#include "tbb/tbb.h"
#include <tbb/compat/thread>

#include "parameter.hpp"
#include "data.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/serialization/utility.hpp>

using namespace boost::posix_time;
using std::cout; using std::endl; 
using std::pair;
using std::priority_queue;
using std::map;
using std::unordered_set;
using std::set;

using tbb::blocked_range;
using tbb::parallel_for;
using tbb::atomic;

namespace solar {
  
  struct aux_info {
    int col_index;
    int row_index;
    scalar xi;
    scalar score;
    scalar sum_loss;
  };

  scalar logistic_loss(scalar x) {
    if (x < 0) {
      return log(1 + exp(x));
    }
    else {
      return log(1 + exp(-x)) + x;
    }
  };
  
  scalar hinge_loss(scalar x) {
    if (x > -1) {
      return x + 1;
    }
    else {
      return 0;
    }
  }
  
  scalar logistic_der(scalar x) {
    return 1.0/(1 + exp(-x));
  }
  
  scalar hinge_der(scalar x) {
    if (x > -1) {
      return 1;
    }
    else {
      return 0;
    }
  }
  
  scalar cont_warp_loss(scalar rank) {
    return log(rank) + 0.5772156649;
  }

  class Model {

  private:
    scalar calc_loss(scalar x) {
      if (param_.loss_type_ == LossType::LOGISTIC) {
        return logistic_loss(x);
      }
      else if (param_.loss_type_ == LossType::HINGE) {
        return hinge_loss(x);
      }
      else {
        cerr << "unsupported loss type" << endl;
        exit(1);
      }
    }

    scalar calc_der(scalar x) {
      if (param_.loss_type_ == LossType::LOGISTIC) {
        return logistic_der(x);
      }
      else if (param_.loss_type_ == LossType::HINGE) {
        return hinge_der(x);
      }
      else {
        cerr << "unsupported loss type" << endl;
        exit(1);
      }
    }

  public:
    Model(parameter& param, Data& data) :
      param_(param),
      data_(data),
      rng_(param.random_seed_)
    {
    }

    ~Model() {
    }
    
    void run();

  private:
    parameter& param_;
    Data& data_;

    rng_type rng_;
    
  };

}

struct pos_index_score {
  int pos_;
  int index_;
  solar::scalar score_;
  pos_index_score(int pos, int index, solar::scalar score) :
    pos_(pos),
    index_(index),
    score_(score) {
    ;
  }
};

void writelog(const char *str) {
  cout << "\n" 
       << second_clock::local_time() << " - " 
       << str << endl;  
}

void solar::Model::run() {
  
  writelog("start run()");

  // define necessary constants
  const int dim = param_.latent_dim_;
  const int num_rows = data_.get_num_rows();
  const unsigned int num_cols = data_.get_num_cols();

  const int numtasks = param_.numtasks_;
  const int numrows_per_part = data_.numrows_per_part_;
  const int numthreads = param_.numthreads_;
  const int numparts = numthreads * numtasks;
  const int rank = param_.rank_;

  const int sgd_num = param_.sgd_num_;
  
  auto& train_col_nnzs = data_.train_col_nnzs_;
  auto& train_row_nnzs = data_.train_row_nnzs_;

  const size_t train_num_points = data_.train_total_nnz_;

  vector<vector<index_type>>& csc_indices = data_.csc_indices_;
  vector<vector<index_type>>& csc_ptrs = data_.csc_ptrs_;
  vector<index_type>& local_nnz = data_.local_nnz_;

  const auto& test_levels = param_.test_levels_;
  const unsigned int max_test_level = *std::max_element(test_levels.begin(), test_levels.end());

  // create thread-specific RNGs
  rng_type *rngs = sallocator<rng_type>().allocate(numthreads);
  for (int i=0; i < numthreads; i++) {
    sallocator<rng_type>().construct(rngs + i, param_.random_seed_ + 15791 * i + 373 * rank);
  }
  rng_type main_rng(param_.random_seed_ + 377 * rank);

  /////////////////////////////////////////////////////////////////////
  // Initialize Parameters
  /////////////////////////////////////////////////////////////////////

  // indexed by thread, local_row_index, coordinate
  scalar **matrix_U = sallocator<scalar *>().allocate(numthreads);
  for (int i=0; i < numthreads; i++) {
    matrix_U[i] = sallocator<scalar>().allocate(numrows_per_part * dim);
  }

  const int numcols_per_part = num_cols / numparts 
      + ((num_cols % numparts > 0) ? 1 : 0);

  scalar *matrix_V = 
    sallocator<scalar>().allocate(static_cast<size_t>(numthreads) 
				  * numcols_per_part * dim);
  // maps local_col_index to global_col_index
  int *col_indices = sallocator<int>().allocate(numthreads * numcols_per_part);
  
  {
    // BUGBUG: figure out what is better, later
    // std::uniform_real_distribution<> init_dist(0, 1.0/sqrt(dim));
    std::uniform_real_distribution<> init_dist(0, 1.0);

    std::thread* init_threads = callocator<std::thread>().allocate(numthreads);

    std::function<void(int)> init_func = 
      [&](int thread_index)->void {
      for (int j=0; j < numrows_per_part * dim; j++) {
	matrix_U[thread_index][j] = init_dist(rngs[thread_index]);
      }
      scalar *local_matrix_V = matrix_V + static_cast<size_t>(thread_index) * numcols_per_part * dim;
      for (int j=0; j < numcols_per_part * dim; j++) {
	local_matrix_V[j] = init_dist(rngs[thread_index]);
      }
      int *local_col_indices = col_indices + thread_index * numcols_per_part;
      for (int j=0; j < numcols_per_part; j++) {
	// rank * numthreads + thread_index = part_index
	int col_index = (rank * numthreads + thread_index) * numcols_per_part + j;
	if (col_index < static_cast<int>(num_cols)) {
	  local_col_indices[j] = col_index;
	}
	else {
	  local_col_indices[j] = -1;
	}
      }
    };

    for (int i=0; i < numthreads; i++) {
      callocator<std::thread>().construct(init_threads + i, 
					  init_func, i);
    }

    for (int i=0; i < numthreads; i++) {
      init_threads[i].join();
    }

    for (int i=0; i < numthreads; i++) {
      callocator<std::thread>().destroy(init_threads + i);
    }
    callocator<std::thread>().deallocate(init_threads, numthreads);

  }

  const int num_cols_upbd = static_cast<size_t>(numthreads) * numcols_per_part * numtasks;

  scalar *global_matrix_V = 
    sallocator<scalar>().allocate(num_cols_upbd * dim);
  int *global_col_indices = 
    sallocator<int>().allocate(num_cols_upbd);

  MPI_Allgather(matrix_V, static_cast<size_t>(numthreads) * numcols_per_part * dim, MPI_SCALAR,
		global_matrix_V, static_cast<size_t>(numthreads) * numcols_per_part * dim, MPI_SCALAR,
		MPI_COMM_WORLD);
  MPI_Allgather(col_indices, static_cast<size_t>(numthreads) * numcols_per_part, MPI_INT,
		global_col_indices, static_cast<size_t>(numthreads) * numcols_per_part, MPI_INT,
		MPI_COMM_WORLD);
		
  /////////////////////////////////////////////////////////////////////
  // Calculate Performance of Popularity-based Predictions
  /////////////////////////////////////////////////////////////////////
  {
    writelog("calculate metrics for popularity-based prediction");

    // atomic variables to count success
    atomic<int> *positive_counts = sallocator< atomic<int> >().allocate(test_levels.size());
    for (unsigned int i=0; i < test_levels.size(); i++) {
      sallocator< atomic<int> >().construct(positive_counts+i);
    }
    std::fill_n(positive_counts, test_levels.size(), 0);
    
    atomic<int> num_test_entries;
    num_test_entries = 0;
    atomic<int> num_test_rows;
    num_test_rows = 0;

    std::function<void(int)> popularity_func = [&](int thread_index)->void {

      for (int row_index=0; row_index < numrows_per_part; row_index++) {	
	auto& train_set = data_.train_rowwise_[thread_index][row_index];
	auto& test_set = data_.test_rowwise_[thread_index][row_index];
	// BUGBUG: arbitrarily cutoff users without more than 10 items in the training set
	if (train_set.size() <= 10) {
	  continue;
	}
	if (test_set.size() <= 0) {
	  continue;
	}

	num_test_rows++;

	// the goal here is to get columns with highest popularities
	map< index_type, unsigned int > top_column_ranks;

	priority_queue< pair<int, int>, std::vector<pair<int,int> >,
			std::greater<pair<int,int> > > top_elements;

	for (unsigned int i=0; i < num_cols; i++) {
	  if (train_set.find(i) == train_set.end()) {
	    top_elements.push(pair<int, int>(train_col_nnzs[i], i));
	    if (top_elements.size() > max_test_level) {
	      top_elements.pop();
	    }
	  }
	}

	// build column_index -> rank mapping
	int column_rank = top_elements.size() - 1;
	while (!top_elements.empty()) {
	  top_column_ranks[top_elements.top().second] = column_rank;
	  column_rank--;
	  top_elements.pop();
	}

	// iterate for each item in the test set
	for (int col_index : test_set) {

	  // keep the count
	  num_test_entries++;

	  // find the rank of an item (column)
	  auto found_rank_itr = top_column_ranks.find(col_index);
	  // if the rank is smaller than the highest test level,
	  // update the positive counts
	  if (found_rank_itr != top_column_ranks.end()) {
	    unsigned int found_rank = found_rank_itr->second;

	    for (unsigned int level_index=0; level_index < test_levels.size();
		 level_index++) {
	      unsigned int test_level = test_levels[level_index];
	      if (found_rank < test_level) {
		positive_counts[level_index]++;
	      }
	    }
	  }

	} // end of iteration on each item in test set

      } // end of row_index iteration

      return;
    };

    // calculate metric in each machine
    std::thread* popularity_threads = callocator<std::thread>().allocate(numthreads);
    for (int i=0; i < numthreads; i++) {
      callocator<std::thread>().construct(popularity_threads + i, popularity_func, i);
    }

    for (int i=0; i < numthreads; i++) {
      popularity_threads[i].join();
    }

    // Now we have to gather results from all machines!
    // To MPI_Reduce, first copy the content of atomic variables to plain variables
    int plain_positive_counts[test_levels.size()];
    std::copy(positive_counts, positive_counts + test_levels.size(),
	      plain_positive_counts);
    int plain_num_test_entries = num_test_entries;
    int plain_num_test_rows = num_test_rows;

    int sum_positive_counts[test_levels.size()];
    int sum_num_test_entries = 0;
    int sum_num_test_rows = 0;

    MPI_Reduce(plain_positive_counts, sum_positive_counts, test_levels.size(),
	       MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&plain_num_test_entries, &sum_num_test_entries, 1,
	       MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&plain_num_test_rows, &sum_num_test_rows, 1,
	       MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    // now report gathered results
    if (rank == 0){
      for (unsigned int level_index=0; level_index < test_levels.size();
	   level_index++) {
	unsigned int test_level = test_levels[level_index];
	double precision = static_cast<double>(sum_positive_counts[level_index])
	  /test_level/sum_num_test_rows;
	double recall = static_cast<double>(sum_positive_counts[level_index])
	  /sum_num_test_entries;
	cout << "popularity precision @" << test_level << ": " << precision << 
          ", recall @" << test_level << ": " << recall << endl;
      }
    }
    
    for (unsigned int i=0; i < test_levels.size(); i++) {
      sallocator< atomic<int> >().destroy(positive_counts+i);
    }
    sallocator< atomic<int> >().deallocate(positive_counts, test_levels.size());

    for (int i=0; i < numthreads; i++) {
      callocator<std::thread>().destroy(popularity_threads + i);
    }  
    callocator<std::thread>().deallocate(popularity_threads, numthreads);


  }

  /////////////////////////////////////////////////////////////////////
  // Optimization
  /////////////////////////////////////////////////////////////////////

  int *col_locations = sallocator<int>().allocate(num_cols);

  // prepare auxiliary information variables
  aux_info **auxs = sallocator<aux_info *>().allocate(numthreads);
  vector<vector<vector<aux_info *>>> rowwise_auxptrs 
    = vector<vector<vector<aux_info *>>>(numthreads, vector<vector<aux_info *>>(numrows_per_part, vector<aux_info *>()));
  for (int thread_index=0; thread_index < numthreads; thread_index++) {
    auxs[thread_index] = sallocator<aux_info>().allocate(local_nnz[thread_index]);

    aux_info *ptr = auxs[thread_index];
    for (int col_index=0; col_index < static_cast<int>(num_cols); col_index++) {
      for (int j = csc_ptrs[thread_index][col_index]; j < csc_ptrs[thread_index][col_index+1]; j++) {
	int row_index = csc_indices[thread_index][j];
	ptr->col_index = col_index;
	ptr->row_index = row_index;
	ptr->xi = 1.0;
	rowwise_auxptrs[thread_index][row_index].push_back(ptr);
	ptr++;
      }
    }
  }

  double stepsize = param_.learning_rate_ / (num_cols - 1);
  const double reg = param_.regularization_;

  writelog("starts optimization");

  // scalar prev_obj_value = 0.0;
  
  int *global_perm = sallocator<int>().allocate(num_cols_upbd);
  int *local_perm = sallocator<int>().allocate(numcols_per_part * numthreads);

  double cumul_computation_time = 0.0;
  
  for (int iter_num=0; iter_num < param_.max_iteration_; iter_num++) {
    
    std::stringstream monitor_stream;
    monitor_stream << iter_num << "," << param_ << ",";

    cout << "iteration: " << iter_num << endl;

    if ( (param_.dump_prefix_.length() > 0) &&
         (iter_num % param_.dump_period_ == 0)) {

      cout << "dumping data" << endl;

      {
        std::ofstream ofile(param_.dump_prefix_ + "_user_" + boost::lexical_cast<std::string>(iter_num) +
                            "_" + boost::lexical_cast<std::string>(rank) +
                            ".txt");

        // matrix_U[i] = sallocator<scalar>().allocate(numrows_per_part * dim);
        int current_row_index = data_.row_start_index_;
        for (int thread_index=0; thread_index < numthreads; thread_index++) {
          scalar *user_param = matrix_U[thread_index];
          for (int i=0; i < numrows_per_part; i++) {
            int global_row_index = data_.row_perm_inv_[current_row_index];
	    // do not print outside of valid area
	    if (current_row_index < num_rows) {
	      ofile << (global_row_index+1);
	      for (int i=0; i < dim; i++) {
		ofile << ",";
		ofile << user_param[i];
	      }
	      ofile << endl;
	      user_param += dim;
	    }
            current_row_index++;
          }
        }

        ofile.close();
      }

      if (rank == 0) {
        std::ofstream ofile(param_.dump_prefix_ + "_item_" + boost::lexical_cast<std::string>(iter_num) +
                            ".txt");
        scalar *item_param = global_matrix_V;
        for (unsigned int item_index = 0; item_index < num_cols; item_index++) {
          ofile << (global_col_indices[item_index]+1);
          for (int i=0; i < dim; i++) {
            ofile << ",";
            ofile << item_param[i];
          }
          ofile << endl;
          item_param += dim;
        }
        
        ofile.close();
      }
    }

    {
      writelog("calculate metrics");

      // atomic variables to count success
      atomic<int> *positive_counts = sallocator< atomic<int> >().allocate(test_levels.size());
      for (unsigned int i=0; i < test_levels.size(); i++) {
	sallocator< atomic<int> >().construct(positive_counts+i);
      }
      std::fill_n(positive_counts, test_levels.size(), 0);
    
      atomic<int> num_test_entries;
      num_test_entries = 0;
      atomic<int> num_test_rows;
      num_test_rows = 0;

      std::function<void(int)> metric_func = [&](int thread_index)->void {

	for (int row_index=0; row_index < numrows_per_part; row_index++) {	

	  auto& train_set = data_.train_rowwise_[thread_index][row_index];
	  auto& test_set = data_.test_rowwise_[thread_index][row_index];
	  // BUGBUG: arbitrarily cutoff users without more than 10 items in the training set
	  if (train_set.size() <= 10) {
	    continue;
	  }
	  if (test_set.size() <= 0) {
	    continue;
	  }

	  num_test_rows++;

	  // the goal here is to get columns with highest popularities
	  map< index_type, unsigned int > top_column_ranks;

	  priority_queue< pair<scalar, int>, std::vector<pair<scalar,int> >, std::greater<pair<scalar,int> > > top_elements;

	  for (int i=0; i < num_cols_upbd; i++) {
	    int col_index = global_col_indices[i];

	    if (col_index < 0) {
	      continue;
	    }

	    if (train_set.find(col_index) == train_set.end()) {
	      scalar *row_vec = matrix_U[thread_index] + row_index * dim;
	      scalar *col_vec = global_matrix_V + dim * i;
	      scalar org_dot = std::inner_product(row_vec, row_vec + dim, col_vec, 0.0);

	      top_elements.push(pair<scalar, int>(org_dot, col_index));
	      if (top_elements.size() > max_test_level) {
		top_elements.pop();
	      }
	    }
	  }

	  // build column_index -> rank mapping
	  int column_rank = top_elements.size() - 1;
	  while (!top_elements.empty()) {
	    top_column_ranks[top_elements.top().second] = column_rank;
	    column_rank--;
	    top_elements.pop();
	  }

	  // iterate for each item in the test set
	  for (int col_index : test_set) {

	    // keep the count
	    num_test_entries++;

	    // find the rank of an item (column)
	    auto found_rank_itr = top_column_ranks.find(col_index);
	    // if the rank is smaller than the highest test level,
	    // update the positive counts
	    if (found_rank_itr != top_column_ranks.end()) {
	      unsigned int found_rank = found_rank_itr->second;
	      
	      for (unsigned int level_index=0; level_index < test_levels.size();
		   level_index++) {
		unsigned int test_level = test_levels[level_index];
		if (found_rank < test_level) {
		  positive_counts[level_index]++;
		}
	      }
	    }

	  } // end of iteration on each item in test set

	} // end of row_index iteration

	return;
      };

      // calculate metric in each machine
      std::thread* metric_threads = callocator<std::thread>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().construct(metric_threads + i, metric_func, i);
      }
      
      for (int i=0; i < numthreads; i++) {
	metric_threads[i].join();
      }

      // Now we have to gather results from all machines!
      // To MPI_Reduce, first copy the content of atomic variables to plain variables
      int plain_positive_counts[test_levels.size()];
      std::copy(positive_counts, positive_counts + test_levels.size(),
		plain_positive_counts);
      int plain_num_test_entries = num_test_entries;
      int plain_num_test_rows = num_test_rows;

      int sum_positive_counts[test_levels.size()];
      int sum_num_test_entries = 0;
      int sum_num_test_rows = 0;
      
      MPI_Reduce(plain_positive_counts, sum_positive_counts, test_levels.size(),
		 MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
      MPI_Reduce(&plain_num_test_entries, &sum_num_test_entries, 1,
		 MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
      MPI_Reduce(&plain_num_test_rows, &sum_num_test_rows, 1,
		 MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

      // now report gathered results
      if (rank == 0){
	for (unsigned int level_index=0; level_index < test_levels.size();
	     level_index++) {
	  unsigned int test_level = test_levels[level_index];
	  double precision = static_cast<double>(sum_positive_counts[level_index])
	    /test_level/sum_num_test_rows;
	  double recall = static_cast<double>(sum_positive_counts[level_index])
	    /sum_num_test_entries;
	  cout << "precision @" << test_level << ": " << precision << ", recall @"
	       << test_level << ": " << recall << endl;
	  monitor_stream << precision << "," << recall << ",";
	}
      }
	
      for (unsigned int i=0; i < test_levels.size(); i++) {
	sallocator< atomic<int> >().destroy(positive_counts+i);
      }
      sallocator< atomic<int> >().deallocate(positive_counts, test_levels.size());
      
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().destroy(metric_threads + i);
      }  
      callocator<std::thread>().deallocate(metric_threads, numthreads);

    } // end of metric calculation    


    tbb::tick_count compute_start_time = tbb::tick_count::now();

    // sample a new assignment of column indices
    if (rank == 0) {
      std::iota(global_perm, global_perm + num_cols_upbd, 0);
      std::shuffle(global_perm, global_perm + num_cols_upbd,
		   main_rng);
    }
    MPI_Scatter(global_perm, numcols_per_part * numthreads, MPI_INT,
		local_perm, numcols_per_part * numthreads, MPI_INT, 0,
		MPI_COMM_WORLD);

    // copy global parameters to local space
    {
      scalar *target_ptr = matrix_V;
      for (int i=0; i < numcols_per_part * numthreads; i++) {
	// copy the index
	col_indices[i] = global_col_indices[local_perm[i]];

	// copy the parameter value
	scalar *source_ptr = global_matrix_V + local_perm[i] * dim;
	std::copy(source_ptr, source_ptr + dim, target_ptr);
	target_ptr += dim;
      }
    }

    // inside a local machine, we use a permutation matrix 
    // to determine the order of block assignment
    vector<vector<int> > perm_matrix(numthreads, vector<int>(numthreads,0));
    {
      vector<int> col_perm(numthreads, 0);
      vector<int> tmp_buffer(numthreads, 0);

      for(int i=0; i < numthreads; i++){
	col_perm[i] = i;
	for(int j=0; j < numthreads; j++){
	  perm_matrix[i][j]=(i+j) % numthreads;
	}
      }
      shuffle(perm_matrix.begin(), perm_matrix.end(), main_rng);
      shuffle(col_perm.begin(), col_perm.end(), main_rng);
      // apply column permutation
      for(int i=0; i < numthreads; i++){
	copy(perm_matrix[i].begin(), perm_matrix[i].end(), tmp_buffer.begin());
	for (int j=0; j < numthreads; j++) {
	  perm_matrix[i][col_perm[j]] = tmp_buffer[j];
	}
      }
    }

    // run SGD
    for (int inner_iter=0; inner_iter < numthreads; inner_iter++) {

      std::function<void(int)> sgd_func = [&](int thread_index)->void {      
	
	int chunk_index = perm_matrix[inner_iter][thread_index];

	int *local_col_indices = col_indices + chunk_index * numcols_per_part;
	scalar *local_matrix_V = matrix_V + static_cast<size_t>(chunk_index) * numcols_per_part * dim;

	std::uniform_int_distribution<> init_dist(0, numcols_per_part-1);
	auto sample_column = [&](int local_col_index)->int {
	  int ret=local_col_index;
	  while (true) {
	    ret = init_dist(rngs[thread_index]);
	    if ((ret != local_col_index) && (local_col_indices[ret] != -1)) {
	      return ret;
	    }
	  }
	};

	scalar backup_col_vec[dim];
	scalar backup_sam_vec[dim];

	if (param_.loss_type_ == LossType::LOGISTIC) {

	  for (int sgd_iter = 0; sgd_iter < sgd_num; sgd_iter++) {
	    for (int i=0; i < numcols_per_part; i++) {
	      int col_index = local_col_indices[i];
	      scalar *col_vec = local_matrix_V + i * dim;
	      
	      if (col_index < 0) {
		continue;
	      }

	      for (int j = csc_ptrs[thread_index][col_index]; j < csc_ptrs[thread_index][col_index+1]; j++) {
		aux_info& aux_ptr = auxs[thread_index][j];
		int row_index = aux_ptr.row_index;
		scalar xi = aux_ptr.xi;
		scalar *row_vec = matrix_U[thread_index] + row_index * dim;
		
		// cout << "here: " << j << "," << csc_ptrs[thread_index][col_index] << "," << csc_ptrs[thread_index][col_index+1] << endl;
		// cout << "i: " << i << "," << numcols_per_part << endl;
		
		int sam_local_index = sample_column(i);
		int sam_index = local_col_indices[sam_local_index];
		scalar *sam_vec = local_matrix_V + sam_local_index * dim;
	      
		std::copy(col_vec, col_vec + dim, backup_col_vec);
		std::copy(sam_vec, sam_vec + dim, backup_sam_vec);

		scalar org_dot = std::inner_product(row_vec, row_vec + dim, col_vec, 0.0);
		scalar sam_dot = std::inner_product(row_vec, row_vec + dim, sam_vec, 0.0);

		scalar coef = xi * (num_cols-1) * calc_der(sam_dot - org_dot);
		
		for (int t=0; t < dim; t++) {
		  col_vec[t] -= stepsize * (0.5*reg/train_col_nnzs[col_index]*col_vec[t]
					    - coef*row_vec[t]);
		  sam_vec[t] -= stepsize * (0.5*reg/(train_num_points-train_col_nnzs[sam_index])*sam_vec[t]
					    + coef*row_vec[t]);
		  row_vec[t] -= stepsize * (reg/train_row_nnzs[thread_index][row_index]*row_vec[t]
					    + coef*(backup_sam_vec[t]-backup_col_vec[t]) );
		}
	      }
	    }
	  }
	} // case of logistic loss (end)
	else if (param_.loss_type_ == LossType::HINGE) {	
	
	  for (int i=0; i < numcols_per_part; i++) {
	    int col_index = local_col_indices[i];
	    scalar *col_vec = local_matrix_V + i * dim;
	      
	    if (col_index < 0) {
	      continue;
	    }

	    for (int j = csc_ptrs[thread_index][col_index]; j < csc_ptrs[thread_index][col_index+1]; j++) {
	      aux_info& aux_ptr = auxs[thread_index][j];
	      int row_index = aux_ptr.row_index;
	      scalar xi = aux_ptr.xi;
	      scalar *row_vec = matrix_U[thread_index] + row_index * dim;

	      for (int sam_iter=0; sam_iter < numcols_per_part; sam_iter++) {
		
		int sam_local_index = sample_column(i);
		int sam_index = local_col_indices[sam_local_index];
		scalar *sam_vec = local_matrix_V + sam_local_index * dim;
	      
		std::copy(col_vec, col_vec + dim, backup_col_vec);
		std::copy(sam_vec, sam_vec + dim, backup_sam_vec);

		scalar org_dot = std::inner_product(row_vec, row_vec + dim, col_vec, 0.0);
		scalar sam_dot = std::inner_product(row_vec, row_vec + dim, sam_vec, 0.0);

		// if hinge loss is nonzero
		if (sam_dot > org_dot - 1) {

		  scalar coef = xi * calc_der(sam_dot - org_dot)
		    * (numcols_per_part-1) / (sam_iter + 1);
		
		  // regularization is tricky. maybe we should just do projection?
		  for (int t=0; t < dim; t++) {
		    col_vec[t] -= stepsize * (0.5*reg/train_col_nnzs[col_index]*col_vec[t]
					      - coef*row_vec[t]);
		    sam_vec[t] -= stepsize * (0.5*reg/(train_num_points-train_col_nnzs[sam_index])*sam_vec[t]
					      + coef*row_vec[t]);
		    row_vec[t] -= stepsize * (reg/train_row_nnzs[thread_index][row_index]*row_vec[t]
					      + coef*(backup_sam_vec[t]-backup_col_vec[t]) );
		  }

		  // geometric sampling is done!
		  break;
		}

	      } // end of sam_iter
	    }
	  }
	  

	}
	else {
	  cerr << "unsupported loss type 1" << endl;
	  exit(1);
	}

      };
      
      std::thread* sgd_threads = callocator<std::thread>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().construct(sgd_threads + i, sgd_func, i);
      }

      for (int i=0; i < numthreads; i++) {
	sgd_threads[i].join();
      }

      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().destroy(sgd_threads + i);
      }  
      callocator<std::thread>().deallocate(sgd_threads, numthreads);

    }

    // at the end of the iteration, synchronize parameters
    MPI_Allgather(matrix_V, static_cast<size_t>(numthreads) * numcols_per_part * dim, MPI_SCALAR,
		  global_matrix_V, static_cast<size_t>(numthreads) * numcols_per_part * dim, MPI_SCALAR,
		  MPI_COMM_WORLD);
    MPI_Allgather(col_indices, static_cast<size_t>(numthreads) * numcols_per_part, MPI_INT,
		  global_col_indices, static_cast<size_t>(numthreads) * numcols_per_part, MPI_INT,
		  MPI_COMM_WORLD);

    for (int i=0; i < num_cols_upbd; i++) {
      if (global_col_indices[i] < 0) {
	continue;
      }
      col_locations[global_col_indices[i]] = i;
    }

    // update auxiliary parameters exactly
    if (iter_num % param_.xi_period_ == 0) {

      writelog("update auxiliary parameters");

      scalar *local_log_sums = sallocator<scalar>().allocate(numthreads);
      std::fill_n(local_log_sums, numthreads, 0.0);
      
      std::function<void(int)> aux_func = [&](int thread_index)->void {      
	for (int row_index=0; row_index < numrows_per_part; row_index++) {

	  scalar *row_vec = matrix_U[thread_index] + row_index * dim;

	  for (aux_info *ptr : rowwise_auxptrs[thread_index][row_index]) {
	    int col_index = ptr->col_index;
	    scalar *col_vec = global_matrix_V + dim * col_locations[col_index];

	    scalar org_dot = std::inner_product(row_vec, row_vec + dim, col_vec, 0.0);
	    ptr->score = org_dot;
	    ptr->sum_loss = 1.0;
	  }

	  scalar *sam_vec = global_matrix_V;
	  for (int i=0; i < num_cols_upbd; i++) {
	    if (global_col_indices[i] >= 0) {
	      scalar sam_dot = std::inner_product(row_vec, row_vec + dim, sam_vec, 0.0);

	      for (aux_info *ptr : rowwise_auxptrs[thread_index][row_index]) {
		int col_index = ptr->col_index;
		if (col_index != global_col_indices[i]) {
		  ptr->sum_loss += calc_loss(sam_dot - ptr->score);
		}
	      }
	    }
	    sam_vec += dim;
	  }

	  for (aux_info *ptr : rowwise_auxptrs[thread_index][row_index]) {
	    local_log_sums[thread_index] += log(ptr->sum_loss);
	  }

	}

      };

      std::thread* aux_threads = callocator<std::thread>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().construct(aux_threads + i, aux_func, i);
      }

      for (int i=0; i < numthreads; i++) {
	aux_threads[i].join();
      }

      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().destroy(aux_threads + i);
      }  
      callocator<std::thread>().deallocate(aux_threads, numthreads);

      scalar machine_log_sum = std::accumulate(local_log_sums, local_log_sums + numthreads,
					       0.0);

      scalar global_log_sum = 0.0;

      MPI_Allreduce(&machine_log_sum, &global_log_sum, 1,
		    MPI_SCALAR, MPI_SUM, MPI_COMM_WORLD);
      
      scalar mean_log_sum = global_log_sum / train_num_points;

      // now normalize!
      std::function<void(int)> norm_func = [&](int thread_index)->void {      
	for (int i=0; i < local_nnz[thread_index]; i++) {
	  auxs[thread_index][i].xi = exp(mean_log_sum - log(auxs[thread_index][i].sum_loss));
	}
      };
	      
      std::thread* norm_threads = callocator<std::thread>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().construct(norm_threads + i, norm_func, i);
      }

      for (int i=0; i < numthreads; i++) {
	norm_threads[i].join();
      }

      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().destroy(norm_threads + i);
      }  
      callocator<std::thread>().deallocate(norm_threads, numthreads);

      sallocator<scalar>().deallocate(local_log_sums, numthreads);

    }

    double computation_time = (tbb::tick_count::now() - compute_start_time).seconds();
    cumul_computation_time += computation_time;
    monitor_stream << cumul_computation_time << ",";

    // approximate the objective function
    {
      writelog("approximate the objective function");
      int sample_num_per_part = param_.sample_num_ / numparts;
      int total_sample_num = sample_num_per_part * numparts;
      
      scalar *local_loss_sums = sallocator<scalar>().allocate(numthreads);
      std::fill_n(local_loss_sums, numthreads, 0.0);

      scalar *local_reg_sums = sallocator<scalar>().allocate(numthreads);
      std::fill_n(local_reg_sums, numthreads, 0.0);

      std::function<void(int)> approx_func = [&](int thread_index)->void {      
	// approximate the loss
	std::uniform_int_distribution<> approx_dist(0, local_nnz[thread_index] - 1);
	for (int i=0; i < sample_num_per_part; i++) {
	  int approx_index = approx_dist(rngs[thread_index]);
	  int row_index = auxs[thread_index][approx_index].row_index;
	  int col_index = auxs[thread_index][approx_index].col_index;
	  scalar xi = auxs[thread_index][approx_index].xi;

	  scalar *row_vec = matrix_U[thread_index] + row_index * dim;
	  scalar *col_vec = global_matrix_V + dim * col_locations[col_index];

	  scalar org_dot = std::inner_product(row_vec, row_vec + dim, col_vec, 0.0);
	  scalar loss_sum = 1.0;
	  //int exact_rank=1;

	  for (int sam_index=0; sam_index < num_cols_upbd; sam_index++) {
	    if ((global_col_indices[sam_index]==col_index) || (global_col_indices[sam_index]==-1)) {
	      continue;
	    }
	    scalar *sam_vec = global_matrix_V + dim * sam_index;
	    scalar sam_dot = std::inner_product(row_vec, row_vec + dim, sam_vec, 0.0);
	    loss_sum += calc_loss(sam_dot-org_dot);
	  }
	  local_loss_sums[thread_index] += xi * loss_sum;
	}
	// calculate the regularizer term
	for (int row_index=0; row_index < numrows_per_part; row_index++) {
	  if (rowwise_auxptrs[thread_index][row_index].size() > 0) {
	    scalar *row_vec = matrix_U[thread_index] + row_index * dim;
	    local_reg_sums[thread_index] += std::inner_product(row_vec, row_vec + dim, row_vec, 0.0);
	  }
	}
	int *local_col_indices = col_indices + thread_index * numcols_per_part;
	scalar *local_matrix_V = matrix_V + static_cast<size_t>(thread_index) * numcols_per_part * dim;
	for (int col_index=0; col_index < numcols_per_part; col_index++) {
	  if (local_col_indices[col_index] >= 0) {
	    scalar *col_vec = local_matrix_V + col_index * dim;
	    local_reg_sums[thread_index] += std::inner_product(col_vec, col_vec + dim, col_vec, 0.0);
	  }
	}
      };

      std::thread* approx_threads = callocator<std::thread>().allocate(numthreads);
      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().construct(approx_threads + i, approx_func, i);
      }

      for (int i=0; i < numthreads; i++) {
	approx_threads[i].join();
      }

      for (int i=0; i < numthreads; i++) {
	callocator<std::thread>().destroy(approx_threads + i);
      }  
      callocator<std::thread>().deallocate(approx_threads, numthreads);

      scalar machine_loss_sum = std::accumulate(local_loss_sums, local_loss_sums + numthreads,
						0.0);
      scalar global_loss_sum = 0.0;
      MPI_Allreduce(&machine_loss_sum, &global_loss_sum, 1,
		    MPI_SCALAR, MPI_SUM, MPI_COMM_WORLD);

      scalar machine_reg_sum = std::accumulate(local_reg_sums, local_reg_sums + numthreads,
					       0.0);
      scalar global_reg_sum = 0.0;
      MPI_Allreduce(&machine_reg_sum, &global_reg_sum, 1,
		    MPI_SCALAR, MPI_SUM, MPI_COMM_WORLD);

      scalar approx_obj_value = global_loss_sum * train_num_points / total_sample_num + global_reg_sum;

      sallocator<scalar>().deallocate(local_loss_sums, numthreads);
      sallocator<scalar>().deallocate(local_reg_sums, numthreads);

      monitor_stream << approx_obj_value << endl;
      if (rank == 0) {
	cout << "monitor," << monitor_stream.str() << endl;
      }
      
    }


  } // end of SGD iteration

  sallocator<int>().deallocate(local_perm, numcols_per_part * numthreads);
  sallocator<int>().deallocate(global_perm, num_cols_upbd);
  

  for (int i=0; i < numthreads; i++) {
    sallocator<aux_info>().deallocate(auxs[i], local_nnz[i]);
  }
  sallocator<aux_info *>().deallocate(auxs, numthreads);
  
  sallocator<int>().deallocate(col_locations, num_cols);

  /////////////////////////////////////////////////////////////////////
  // Deallocate Memory
  /////////////////////////////////////////////////////////////////////

  for (int i=0; i < numthreads; i++) {
    sallocator<scalar>().deallocate(matrix_U[i], numrows_per_part * dim);
  }
  sallocator<scalar *>().deallocate(matrix_U, numthreads);

  sallocator<scalar>().deallocate(matrix_V, static_cast<size_t>(numthreads) 
				  * numcols_per_part * dim);

  sallocator<index_type>().deallocate(col_indices, numthreads * numcols_per_part);

  sallocator<scalar>().deallocate(global_matrix_V, num_cols * dim);
  sallocator<int>().deallocate(global_col_indices, num_cols);


  for (int i=0; i < numthreads; i++) {
    sallocator<rng_type>().destroy(rngs + i);
  }
  sallocator<rng_type>().deallocate(rngs, numthreads);

}

#endif
