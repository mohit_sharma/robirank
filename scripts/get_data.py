import urllib
import zipfile
import random

def get_letor():
    letor3_gov_rar_filename= "../data/raw/letor-3.0-gov.zip"
    print "Downloading LETOR-3.0 Gov data..."
    urllib.urlretrieve("http://research.microsoft.com/en-us/um/beijing/projects/letor//LETOR3.0/Gov.rar", letor3_gov_rar_filename)
    letor3_ohsumed_zip_filename = "../data/raw/letor-3.0-ohsumed.zip"
    print "Downloading LETOR-3.0 OHSUMED data..."
    urllib.urlretrieve("http://research.microsoft.com/en-us/um/beijing/projects/letor//LETOR3.0/OHSUMED.zip", letor3_ohsumed_zip_filename)
    print "Extracting LETOR-3.0 OHSUMED data..."
    with zipfile.ZipFile(letor3_ohsumed_zip_filename) as zf:
        zf.extractall("../data/letor-3.0/OHSUMED")

def get_ml1m():
    random.seed(12345)
    ml1m_zip_filename = "../data/raw/ml-1m.zip"
    print "Downloading MovieLens 1M data..."
    urllib.urlretrieve("http://files.grouplens.org/datasets/movielens/ml-1m.zip", movielens_zip_filename)
    print "Extracting MovieLens 1M data..."
    ml1m_zip_file = zipfile.ZipFile(ml1m_zip_filename)
    ml1m_zip_file.extractall("../data/raw/")
    ml1m_ratings = []
    print "Reading MovieLens 1M data..."
    for line in open("../data/raw/ml-1m/ratings.dat"):
        tokens = line.strip().split("::")
        ml1m_ratings.append( ( int(tokens[0]), int(tokens[1]) ) )
    max_user_index = -1
    max_item_index = -1
    print "Preprocessing MovieLens 1M data..."
    random.shuffle(ml1m_ratings)
    ml1m_train_num = int(len(ml1m_ratings) * 0.9)
    ml1m_train_file = open("../data/ml1m/ml1m_train.txt", "w")
    for user_index, item_index in ml1m_ratings[:ml1m_train_num]:
        if user_index > max_user_index:
            max_user_index = user_index
        if item_index > max_item_index:
            max_item_index = item_index
        ml1m_train_file.write("%d %d\n" % (user_index, item_index))
    ml1m_train_file.close()
    ml1m_test_file = open("../data/ml1m/ml1m_test.txt", "w")
    for user_index, item_index in ml1m_ratings[ml1m_train_num:]:
        ml1m_test_file.write("%d %d\n" % (user_index, item_index))
    ml1m_test_file.close()

'''    
def get_msd():
    msd_zip_filename = "../data/raw/msd.zip"
    print "Downloading Million Song Dataset... (this may take some time)"
    #urllib.urlretrieve("http://labrosa.ee.columbia.edu/millionsong/sites/default/files/challenge/train_triplets.txt.zip",
    #                   msd_zip_filename)
    print "Extracting Million Song Dataset..."
    #msd_zip_file = zipfile.ZipFile(msd_zip_filename)
    #msd_zip_file.extractall("../data/raw/msd/")
    user_to_index = {}
    item_to_index = {}
    msd_ratings = []
    max_user_index = -1
    max_item_index = -1
    print "Reading Million Song Dataset..."
    for line in open("../data/raw/msd/train_triplets.txt"):
        user_id, item_id, count = line.split("\t")
        if user_id not in user_to_index:
            user_to_index[user_id] = len(user_to_index) + 1
        user_index = user_to_index[user_id]
        if user_index > max_user_index:
            max_user_index = user_index
        if item_id not in item_to_index:
            item_to_index[item_id] = len(item_to_index) + 1
        item_index = item_to_index[item_id]
        if item_index > max_item_index:
            max_item_index = item_index
        msd_ratings.append( ( user_index, item_index ) )
    print "# users: %d, # items: %d" % (max_user_index, max_item_index)
    print "Preprocessing Million Song Dataset..."
    random.shuffle(msd_ratings)
    msd_train_num = int(len(msd_ratings) * 0.9)
    print "number of training data points: %d, test: %d" % (msd_train_num, len(msd_ratings) - msd_train_num)
    msd_train_file = open("../data/msd/msd_train.txt", "w")
    for user_index, item_index in msd_ratings[:msd_train_num]:
        msd_train_file.write("%d %d\n" % (user_index, item_index))
    msd_train_file.close()
    msd_test_file = open("../data/msd/msd_test.txt", "w")
    for user_index, item_index in msd_ratings[msd_train_num:]:
        msd_test_file.write("%d %d\n" % (user_index, item_index))
    msd_test_file.close()
    msd_user_file = open("../data/msd/msd_user_indices.txt", "w")
    for user_id, user_index in user_to_index.iteritems():
        msd_user_file.write("%s\t%d\n" % (user_id, user_index))
    msd_user_file.close()
    msd_item_file = open("../data/msd/msd_item_indices.txt", "w")
    for item_id, item_index in item_to_index.iteritems():
        msd_item_file.write("%s\t%d\n" % (item_id, item_index))
    msd_item_file.close()
'''
    
get_letor()
get_ml1m()
get_msd()
